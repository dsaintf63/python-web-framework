# Python Web Framework



## Description

Python Web Framework is a Web service written in Python 2 then converted to Python 3.
The web serverice runs according to a configuration file. 
The Configuration file sets all web site's security and database configurations. 
Python is used for dynamic creation of pages which can be based of page templates where simple string replace is done.

Since I wrote this framework, I have never returned to apache for web services again.
I can write entire web aplets in Python. It's greate for a database interface that output json.
There is an automatic authentication where a cookie is given to grant acess to different parts of the site. All configured in the configuration file.

## Summary of what comes with the framework
* Easy configuration in JSON format.
* Dynamic configuration modification based on web applicaiton spcific for the user.
* Threaded SSL Web Service.
* Database schema creation based on configuration: Database is automaticly configured on startup based on the configuration.
* Connection to different databases: SQLLite by default
* Can run background processes pereodicly to assemble complicated pages, pull stock reports,... what ever you dream of.
* Easy deployment. Clone and start.
